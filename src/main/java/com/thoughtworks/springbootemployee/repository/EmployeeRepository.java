package com.thoughtworks.springbootemployee.repository;


import com.thoughtworks.springbootemployee.pojo.Employee;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Repository("employeeRepository")
public class EmployeeRepository {
    private List<Employee> employees;

    public EmployeeRepository(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Employee> getEmployeeList() {
        return employees;
    }

    public Employee getEmployeeById(int id) {
        return employees.stream()
                .filter(employee -> employee.getId() == id).findFirst().orElse(null);
    }

    public List<Employee> getEmployeesByGender(@RequestParam String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public Employee createEmployee(Employee employee) {
        employee.setId(nextId());
        employees.add(employee);
        return employee;
    }

    public Employee updateEmployee(int id, Employee employee) {
        Employee employeeFound = employees.stream()
                .filter(employeeInList -> employeeInList.getId() == id)
                .findFirst()
                .orElse(null);
        return updateEmployee(employeeFound);
    }

    private Employee updateEmployee(Employee employee){
        if (employee != null) {
            if (employee.getAge() != employee.getAge()) {
                employee.setAge(employee.getAge());
            }
            if (employee.getSalary() != employee.getSalary()) {
                employee.setSalary(employee.getSalary());
            }
        }
        return employee;
    }

    public void deleteEmployeeById(int id) {
        employees.stream().filter(employeeInList -> employeeInList.getId() == id)
                .findFirst()
                .ifPresent(employeeInList -> employees.remove(employeeInList));
    }

    public List<Employee> getEmployeePage(int page, int size) {
        return employees.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public List<Employee> getEmployeesByCompanyId(int companyId) {
        return employees.stream().filter(employee -> employee.getCompanyId() == companyId).collect(Collectors.toList());
    }

    private int nextId() {
        int maxId = employees.stream()
                .mapToInt(Employee::getId)
                .max()
                .orElse(0);
        return maxId + 1;
    }
}
