package com.thoughtworks.springbootemployee.pojo;

public class Employee {
    private int id;
    private int age;
    private String name;
    private String gender;
    private double salary;
    private int companyId;

    public Employee(){

    }
    public Employee(int id , String name,int age, String gender, double salary) {
        this.id = id;
        this.age = age;
        this.name = name;
        this.gender = gender;
        this.salary = salary;
    }

    public Employee(int id, int age, String name, String gender, double salary, int companyId) {
        this.id = id;
        this.age = age;
        this.name = name;
        this.gender = gender;
        this.salary = salary;
        this.companyId = companyId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public int getCompanyId() {
        return companyId;
    }
}
