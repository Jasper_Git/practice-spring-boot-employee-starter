package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.pojo.Company;
import com.thoughtworks.springbootemployee.pojo.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequestMapping("/companies")
public class CompanyController {

    @Autowired
    @Qualifier("companyRepository")
    private CompanyRepository companyRepository;

    @Autowired
    @Qualifier("employeeRepository")
    private EmployeeRepository employeeRepository;

    @GetMapping
    public List<Company> getAllCompanies() {
        return companyRepository.getCompanies();
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable int id) {
        return companyRepository.getCompanyById(id);
    }

    @PostMapping
    public Company addCompany(@RequestBody Company company) {
        return companyRepository.addCompany(company);
    }

    @PutMapping("/{id}")
    public Company updateCompany(@PathVariable int id, @RequestBody Company company) {
        return companyRepository.updateCompany(id, company);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> getCompanyPage(@RequestParam int page, @RequestParam int size) {
        return companyRepository.getCompanyPage(page, size);
    }

    @GetMapping("/{companyId}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable int companyId) {
        return employeeRepository.getEmployeesByCompanyId(companyId);
    }

    @DeleteMapping("/{id}")
    public void deleteCompany(@PathVariable int id) {
        companyRepository.deleteCompany(id);
    }
}
