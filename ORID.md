## Objective

- In the morning, we used the concept map to summarize what we had learned, and learned HTTP basics and RESTful APIs.
- In the afternoon, we have a brief understanding of the SpringBoot framework and the advantages of using it, and then use SpringBoot as a basic environment to practice some common web annotations and RESTful.

## Reflective

- Today for me, it is a matter of learning from the past. The first is to summarize and make connections about previous knowledge points, and secondly, HTTP, SpringBoot, and RESTful have all been studied during college. After today, there is a deeper understanding of them.

## Interpretive

- RESTful refers to the presentation layer state transformation of resources.
  - Resources refer to resources on the network, and access URI can access resources
  - The presentation layer refers to the fact that resources have multiple external manifestations, and the form in which the resources are presented is called the presentation layer
  - State transformation means that HTTP is stateless, all states exist on the server, and if you want to manipulate the server's resources, you can use the four common modes of HTTP to make the server state change.
- In class, when students introduced SpringBoot, they introduced some of its advantages, but in fact, SpringBoot also has obvious disadvantages:
  - There is a risk of over-encapsulation, which is not conducive to understanding the underlying code implementation.
  - Since you don't have to manually configure yourself, it is difficult to locate when an error is reported.

## Decisional

- Spending more energy to carefully understand the various iterative versions of HTTP and HTTPS is conducive to future web application development, rather than staying in the basic understanding.